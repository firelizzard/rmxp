package object

import (
	"crypto"
	"errors"
	"fmt"

	"gitlab.com/firelizzard/rmxp/smp"
)

var fieldType = "Type"

type Object interface {
	ID() ID
	Sender() (ID, bool)
	Sign(crypto.PrivateKey) error
}

var objCtors = map[string]func() Object{}

func Unmarshal(m *smp.Message) (Object, error) {
	typ, ok := m.Fields.Get(fieldType)
	if !ok {
		return nil, errors.New("missing Type field")
	}

	ctor, ok := objCtors[typ]
	if !ok {
		return nil, fmt.Errorf("unknown object type %v", typ)
	}

	obj := ctor()
	err := m.Unmarshal(obj)
	return obj, err
}

func Register(typ string, ctor func() Object) {
	if _, ok := objCtors[typ]; ok {
		panic(fmt.Sprintf("object type %v already registered", typ))
	}
	objCtors[typ] = ctor
}

func RegisterType[PT interface {
	~*T
	Object
}, T any](typ string) string {
	Register(typ, func() Object {
		return PT(new(T))
	})
	return typ
}
