package object

import "fmt"

type Store interface {
	Load(PartialID) (Object, error)
	Store(Object) error
	LoadState(string) (Object, error)
	StoreState(string, Object) error
}

func LoadAs[T any](store Store, id PartialID) (T, error) {
	obj, err := store.Load(id)
	var z T
	if err != nil {
		return z, err
	}

	v, ok := obj.(T)
	if !ok {
		return z, fmt.Errorf("%x is not a %T", id[:8], z)
	}
	return v, nil
}

func LoadStateAs[T any](store Store, id string) (T, error) {
	obj, err := store.LoadState(id)
	var z T
	if err != nil {
		return z, err
	}

	v, ok := obj.(T)
	if !ok {
		return z, fmt.Errorf("%s is not a %T", id, z)
	}
	return v, nil
}
