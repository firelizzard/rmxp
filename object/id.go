package object

import (
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"slices"
	"strings"

	"gitlab.com/firelizzard/rmxp/smp"
)

type ID [32]byte

func (id ID) String() string { return hex.EncodeToString(id[:]) }

func (id ID) MarshalText() ([]byte, error) {
	b := make([]byte, 64)
	hex.Encode(b, id[:])
	return b, nil
}

func (id *ID) UnmarshalText(b []byte) error {
	if len(b)%2 != 0 {
		return hex.ErrLength
	}
	if len(b) != 64 {
		return fmt.Errorf("invalid id: want 32, got %d", len(b)/2)
	}
	_, err := hex.Decode((*id)[:], b)
	return err
}

type PartialID []byte

func (id PartialID) String() string { return hex.EncodeToString(id) }

func (id PartialID) MarshalText() ([]byte, error) {
	b := make([]byte, 2*len(id))
	hex.Encode(b, id[:])
	return b, nil
}

func (id *PartialID) UnmarshalText(b []byte) error {
	if len(b)%2 != 0 {
		return hex.ErrLength
	}
	*id = make(PartialID, len(b)/2)
	_, err := hex.Decode((*id)[:], b)
	return err
}

func id(m *smp.Message) ID {
	m = m.Copy()

	slices.SortStableFunc(m.Fields, func(a, b smp.Field) int {
		return strings.Compare(a.Name, b.Name)
	})

	for i := range m.Fields {
		m.Fields[i].Name = strings.ToLower(m.Fields[i].Name)
	}

	b, err := smp.Marshal(m)
	if err != nil {
		panic(fmt.Errorf("unable to marshal message: %w", err))
	}

	return ID(sha256.Sum256(b))
}
