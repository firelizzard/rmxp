package object

import (
	"bytes"
	"fmt"
)

type Memstore struct {
	objects map[ID]Object
	short   map[[2]byte]map[ID]bool
	state   map[string]Object
}

func (m *Memstore) Load(id PartialID) (Object, error) {
	var candidates []PartialID
	if len(id) >= 32 {
		goto resolved
	}

	for c := range m.short[[2]byte(id)] {
		if bytes.HasPrefix(c[:], id) {
			candidates = append(candidates, c[:])
		}
	}
	if len(candidates) == 0 {
		return nil, ErrNotFound.WithFormat("cannot resolve partial ID %v", id)
	}
	if len(candidates) > 1 {
		return nil, &Error{
			Code:    ErrAmbiguous,
			IDs:     candidates,
			Message: fmt.Sprintf("partial ID %v is ambiguous", id),
		}
	}
	id = candidates[0]

resolved:
	obj, ok := m.objects[ID(id)]
	if !ok {
		return nil, ErrNotFound.WithFormat("%v not found", id)
	}
	return obj, nil
}

func (m *Memstore) Store(obj Object) error {
	if m.objects == nil {
		m.objects = map[ID]Object{}
		m.short = map[[2]byte]map[ID]bool{}
	}

	id := obj.ID()
	m.objects[id] = obj

	if m.short[[2]byte(id[:])] == nil {
		m.short[[2]byte(id[:])] = map[ID]bool{}
	}
	m.short[[2]byte(id[:])][id] = true
	return nil
}

func (m *Memstore) LoadState(id string) (Object, error) {
	obj, ok := m.state[id]
	if !ok {
		return nil, ErrNotFound.WithFormat("%v not found", id)
	}
	return obj, nil
}

func (m *Memstore) StoreState(id string, obj Object) error {
	if m.state == nil {
		m.state = map[string]Object{}
	}
	m.state[id] = obj
	return nil
}
