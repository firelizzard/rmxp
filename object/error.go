package object

import (
	"crypto"
	"fmt"
	"strconv"

	"gitlab.com/firelizzard/rmxp/smp"
)

type Error struct {
	base    Base
	Code    ErrorCode
	IDs     []PartialID `smp:"ID"`
	Message string      `smp:"-,body"`
}

var typeError = "error"

func init() { RegisterType[*Error](typeError) }

func (e *Error) ID() ID                           { return e.base.ID(e) }
func (e *Error) Sender() (ID, bool)               { return e.base.Sender() }
func (e *Error) Sign(key crypto.PrivateKey) error { return e.base.Sign(e, key) }
func (e *Error) Error() string                    { return e.Message }
func (e *Error) Unwrap() error                    { return e.Code }

func (e *Error) MarshalSMP() (*smp.Message, error) {
	type E Error
	return e.base.Marshal((*E)(e), typeError)
}

func (e *Error) UnmarshalSMP(m *smp.Message) error {
	*e = Error{}
	type E Error
	return e.base.Unmarshal(m, (*E)(e), typeError)
}

type ErrorCode int

const (
	ErrOk ErrorCode = iota
	ErrUnknown
	ErrInvalid
	ErrAmbiguous
	ErrNotFound
)

func (e ErrorCode) Error() string {
	switch e {
	case ErrOk:
		return "ok"
	case ErrUnknown:
		return "unknown"
	case ErrInvalid:
		return "invalid"
	case ErrAmbiguous:
		return "ambiguous"
	case ErrNotFound:
		return "not found"
	}
	return fmt.Sprintf("unknown error code %d", e)
}

func (e ErrorCode) With(v ...any) *Error {
	return &Error{Code: e, Message: fmt.Sprint(v...)}
}

func (e ErrorCode) WithFormat(message string, v ...any) *Error {
	return &Error{Code: e, Message: fmt.Sprintf(message, v...)}
}

func (e ErrorCode) MarshalText() ([]byte, error) {
	return []byte(strconv.FormatInt(int64(e), 10)), nil
}

func (e *ErrorCode) UnmarshalText(b []byte) error {
	v, err := strconv.ParseInt(string(b), 10, 16)
	if err != nil {
		return err
	}
	*e = ErrorCode(v)
	return nil
}
