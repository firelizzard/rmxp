package object

import (
	"crypto"
	"crypto/sha256"
	"fmt"
	"strings"

	"gitlab.com/firelizzard/rmxp/smp"
)

type Base struct {
	id       ID
	original *smp.Message
}

func (o *Base) Sender() (ID, bool) {
	if o.original == nil ||
		o.original.Signature == nil ||
		o.original.Signature.PublicKey == nil {
		return ID{}, false
	}
	return sha256.Sum256(o.original.Signature.PublicKey), true
}

func (o *Base) Sign(v smp.Marshaller, key crypto.PrivateKey) error {
	_, err := v.MarshalSMP()
	if err != nil {
		return fmt.Errorf("cannot marshal message: %w", err)
	}

	return o.original.Sign(key)
}

func (o *Base) ID(v smp.Marshaller) ID {
	_, err := v.MarshalSMP()
	if err != nil {
		panic(fmt.Errorf("cannot marshal message: %w", err))
	}
	return o.id
}

func (o *Base) Marshal(v any, typ string) (*smp.Message, error) {
	if o.original != nil {
		return o.original.Copy(), nil
	}

	var enc smp.Encoder
	var err error
	o.original, err = enc.Marshal(v)
	if err != nil {
		return nil, err
	}

	err = setType(o.original, typ)
	if err != nil {
		panic(err)
	}

	o.id = id(o.original)
	return o.original.Copy(), nil
}

func (o *Base) Unmarshal(m *smp.Message, v any, typ string) error {
	m = m.Copy()
	err := setType(m, typ)
	if err != nil {
		return err
	}

	o.original = m
	o.id = id(m)
	return m.Unmarshal(v)
}

func setType(m *smp.Message, typ string) error {
	for i := range m.Fields {
		f := &m.Fields[i]
		if strings.EqualFold(f.Name, fieldType) {
			if f.Value != typ {
				return fmt.Errorf("wrong object type: want %s, got %s", typ, f.Value)
			}
			return nil
		}
	}
	m.Fields.Add(fieldType, typ)
	return nil
}
