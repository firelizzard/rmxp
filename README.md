# Reliable Message eXchange Protocol

RMXP is a protocol designed for reliable exchange of messages for applications
such as chat where message identity, comes-before relationships, and whether a
client has received a given message are important.