package exchange

import (
	"crypto"

	"gitlab.com/firelizzard/rmxp/message"
	"gitlab.com/firelizzard/rmxp/object"
	"gitlab.com/firelizzard/rmxp/smp"
)

type pullRequest struct {
	base    object.Base
	Message []object.ID
}

type pullResponse struct {
	base     object.Base
	Messages nested[*message.Message] `smp:"-,body"`
}

func (r *pullRequest) Wrap(s message.PullArgs) {
	r.Message = s.Message
}

func (r *pullRequest) Unwrap() message.PullArgs {
	return message.PullArgs{
		Message: r.Message,
	}
}

func (r *pullResponse) Wrap(v []*message.Message)  { r.Messages = v }
func (r *pullResponse) Unwrap() []*message.Message { return r.Messages }

var typePullRequest = object.RegisterType[*pullRequest]("pullRq")
var typePullResponse = object.RegisterType[*pullResponse]("pullRs")

func (e *pullRequest) ID() object.ID  { return e.base.ID(e) }
func (e *pullResponse) ID() object.ID { return e.base.ID(e) }

func (e *pullRequest) Sender() (object.ID, bool)  { return e.base.Sender() }
func (e *pullResponse) Sender() (object.ID, bool) { return e.base.Sender() }

func (e *pullRequest) Sign(key crypto.PrivateKey) error  { return e.base.Sign(e, key) }
func (e *pullResponse) Sign(key crypto.PrivateKey) error { return e.base.Sign(e, key) }

func (e *pullRequest) MarshalSMP() (*smp.Message, error) {
	type E pullRequest
	return e.base.Marshal((*E)(e), typePullRequest)
}

func (e *pullRequest) UnmarshalSMP(m *smp.Message) error {
	*e = pullRequest{}
	type E pullRequest
	return e.base.Unmarshal(m, (*E)(e), typePullRequest)
}

func (e *pullResponse) MarshalSMP() (*smp.Message, error) {
	type E pullResponse
	return e.base.Marshal((*E)(e), typePullResponse)
}

func (e *pullResponse) UnmarshalSMP(m *smp.Message) error {
	*e = pullResponse{}
	type E pullResponse
	return e.base.Unmarshal(m, (*E)(e), typePullResponse)
}
