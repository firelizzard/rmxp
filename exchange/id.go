package exchange

import (
	"bytes"

	"gitlab.com/firelizzard/rmxp/object"
)

type IDSet []object.PartialID

func (s IDSet) Compress(preserve map[[2]byte]bool) IDSet {
	count := map[[2]byte]int{}
	for _, id := range s {
		count[[2]byte(id)]++
	}

	r := make(IDSet, len(s))
	for i, id := range s {
		if preserve[[2]byte(id)] || count[[2]byte(id)] == 1 {
			id = id[:2]
		}
		r[i] = id
	}
	return r
}

func (s IDSet) MarshalText() ([]byte, error) {
	buf := new(bytes.Buffer)
	for _, id := range s {
		b, err := id.MarshalText()
		if err != nil {
			return nil, err
		}
		_, _ = buf.Write(b)
		_, _ = buf.WriteRune('\n')
	}
	return buf.Bytes(), nil
}

func (s *IDSet) UnmarshalText(b []byte) error {
	count := map[[2]byte]int{}
	for len(b) > 0 {
		c := b
		i := bytes.IndexByte(b, '\n')
		switch {
		case i == 0:
			b = b[1:]
			continue
		case i > 0:
			c, b = b[:i], b[i+1:]
		}

		var id object.PartialID
		err := id.UnmarshalText(c)
		if err != nil {
			return object.ErrInvalid.WithFormat("invalid ID: %v", err)
		}
		if len(id) < 2 {
			return object.ErrInvalid.With("invalid ID: too short")
		}

		if len(id) == 2 {
			count[[2]byte(id)]++
			if count[[2]byte(id)] > 1 {
				return object.ErrAmbiguous.WithFormat("ambiguous partial ID %x", id)
			}
		}

		*s = append(*s, id)
	}
	return nil
}
