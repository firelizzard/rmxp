package exchange

import (
	"bytes"
	"errors"
	"fmt"
	"io"

	"gitlab.com/firelizzard/rmxp/object"
	"gitlab.com/firelizzard/rmxp/smp"
)

type nested[T object.Object] []T

func (n nested[T]) MarshalText() ([]byte, error) {
	buf := new(escapeWriter)
	enc := smp.NewEncoder(buf)
	for _, o := range n {
		err := enc.Encode(o)
		if err != nil {
			return nil, err
		}
	}
	return buf.Bytes(), nil
}

func (n *nested[T]) UnmarshalText(b []byte) error {
	var pos, count int
	for {
		i := bytes.IndexByte(b[pos:], '\\')
		if i < 0 {
			break
		}
		i += pos
		if pos > 0 && i > pos {
			copy(b[pos-count:], b[pos:i])
		}
		if i == len(b)-1 {
			return errors.New("trailing backslash")
		}
		switch b[i+1] {
		case 'n':
			b[i-count] = '\n'
		case 'r':
			b[i-count] = '\r'
		default:
			b[i-count] = b[i+1]
		}
		pos = i + 2
		count++
	}

	dec := smp.NewDecoder(bytes.NewReader(b[:pos-count]))
	for {
		m, err := dec.Read()
		if err != nil {
			if errors.Is(err, io.EOF) {
				break
			}
			return err
		}

		o, err := object.Unmarshal(m)
		if err != nil {
			return err
		}
		p, ok := o.(T)
		if !ok {
			return fmt.Errorf("invalid object: want %T, got %T", p, o)
		}

		*n = append(*n, p)
	}
	return nil
}

type escapeWriter struct {
	bytes.Buffer
}

func (w *escapeWriter) Write(b []byte) (int, error) {
	var n int
	for len(b) > 0 {
		i := bytes.IndexAny(b, "\\\n\r")
		if i < 0 {
			_, _ = w.Buffer.Write(b)
			n += len(b)
			break
		}

		w.Buffer.Write(b[:i])
		w.WriteByte('\\')
		switch b[i] {
		case '\n':
			w.WriteByte('n')
		case '\r':
			w.WriteByte('r')
		default:
			w.WriteByte(b[i])
		}
		n += i + 1
		b = b[i+1:]
	}
	return n, nil
}
