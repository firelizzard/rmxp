package exchange

import (
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/firelizzard/rmxp/message"
	"gitlab.com/firelizzard/rmxp/object"
)

func TestNested(t *testing.T) {
	n := nested[*message.Ref]{
		&message.Ref{Head: object.ID{1}},
		&message.Ref{Head: object.ID{2}},
	}
	b, err := n.MarshalText()
	require.NoError(t, err)

	var m nested[*message.Ref]
	require.NoError(t, m.UnmarshalText(b))
	require.Len(t, m, len(n))
	for i, n := range n {
		m := m[i]
		require.Equal(t, n.Head, m.Head)
	}
}
