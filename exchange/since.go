package exchange

import (
	"crypto"

	"gitlab.com/firelizzard/rmxp/message"
	"gitlab.com/firelizzard/rmxp/object"
	"gitlab.com/firelizzard/rmxp/smp"
)

type sinceRequest struct {
	base object.Base
	Ref  string
	Head object.ID
}

type sinceResponse struct {
	base    object.Base
	Objects []object.ID
}

func (r *sinceRequest) Wrap(s message.SinceArgs) {
	r.Ref = s.Ref
	r.Head = s.Head
}

func (r *sinceRequest) Unwrap() message.SinceArgs {
	return message.SinceArgs{
		Ref:  r.Ref,
		Head: r.Head,
	}
}

func (r *sinceResponse) Wrap(v []object.ID)  { r.Objects = v }
func (r *sinceResponse) Unwrap() []object.ID { return r.Objects }

var typeSinceRequest = object.RegisterType[*sinceRequest]("sinceRq")
var typeSinceResponse = object.RegisterType[*sinceResponse]("sinceRs")

func (e *sinceRequest) ID() object.ID  { return e.base.ID(e) }
func (e *sinceResponse) ID() object.ID { return e.base.ID(e) }

func (e *sinceRequest) Sender() (object.ID, bool)  { return e.base.Sender() }
func (e *sinceResponse) Sender() (object.ID, bool) { return e.base.Sender() }

func (e *sinceRequest) Sign(key crypto.PrivateKey) error  { return e.base.Sign(e, key) }
func (e *sinceResponse) Sign(key crypto.PrivateKey) error { return e.base.Sign(e, key) }

func (e *sinceRequest) MarshalSMP() (*smp.Message, error) {
	type E sinceRequest
	return e.base.Marshal((*E)(e), typeSinceRequest)
}

func (e *sinceRequest) UnmarshalSMP(m *smp.Message) error {
	*e = sinceRequest{}
	type E sinceRequest
	return e.base.Unmarshal(m, (*E)(e), typeSinceRequest)
}

func (e *sinceResponse) MarshalSMP() (*smp.Message, error) {
	type E sinceResponse
	return e.base.Marshal((*E)(e), typeSinceResponse)
}

func (e *sinceResponse) UnmarshalSMP(m *smp.Message) error {
	*e = sinceResponse{}
	type E sinceResponse
	return e.base.Unmarshal(m, (*E)(e), typeSinceResponse)
}
