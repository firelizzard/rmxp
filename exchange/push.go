package exchange

import (
	"crypto"

	"gitlab.com/firelizzard/rmxp/message"
	"gitlab.com/firelizzard/rmxp/object"
	"gitlab.com/firelizzard/rmxp/smp"
)

type pushRequest struct {
	base     object.Base
	Ref      string
	Messages nested[*message.Message] `smp:"-,body"`
}

type pushResponse struct {
	base    object.Base
	NewHead nested[*message.Message] `smp:"-,body"`
}

func (r *pushRequest) Wrap(s message.PushArgs) {
	r.Ref = s.Ref
	r.Messages = s.Messages
}

func (r *pushRequest) Unwrap() message.PushArgs {
	return message.PushArgs{
		Ref:      r.Ref,
		Messages: r.Messages,
	}
}

func (r *pushResponse) Wrap(v *message.Message)  { r.NewHead = []*message.Message{v} }
func (r *pushResponse) Unwrap() *message.Message { return r.NewHead[0] }

var typePushRequest = object.RegisterType[*pushRequest]("pushRq")
var typePushResponse = object.RegisterType[*pushResponse]("pushRs")

func (e *pushRequest) ID() object.ID  { return e.base.ID(e) }
func (e *pushResponse) ID() object.ID { return e.base.ID(e) }

func (e *pushRequest) Sender() (object.ID, bool)  { return e.base.Sender() }
func (e *pushResponse) Sender() (object.ID, bool) { return e.base.Sender() }

func (e *pushRequest) Sign(key crypto.PrivateKey) error  { return e.base.Sign(e, key) }
func (e *pushResponse) Sign(key crypto.PrivateKey) error { return e.base.Sign(e, key) }

func (e *pushRequest) MarshalSMP() (*smp.Message, error) {
	type E pushRequest
	return e.base.Marshal((*E)(e), typePushRequest)
}

func (e *pushRequest) UnmarshalSMP(m *smp.Message) error {
	*e = pushRequest{}
	type E pushRequest
	return e.base.Unmarshal(m, (*E)(e), typePushRequest)
}

func (e *pushResponse) MarshalSMP() (*smp.Message, error) {
	type E pushResponse
	return e.base.Marshal((*E)(e), typePushResponse)
}

func (e *pushResponse) UnmarshalSMP(m *smp.Message) error {
	*e = pushResponse{}
	type E pushResponse
	return e.base.Unmarshal(m, (*E)(e), typePushResponse)
}
