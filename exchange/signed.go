package exchange

import (
	"context"
	"crypto"

	"gitlab.com/firelizzard/rmxp/object"
)

type SignedCaller struct {
	Inner Caller
	Key   crypto.PrivateKey
}

func (s *SignedCaller) Call(ctx context.Context, rq object.Object) (object.Object, error) {
	rs, err := s.Inner.Call(ctx, rq)
	if err != nil {
		return nil, err
	}

	err = rs.Sign(s.Key)
	if err != nil {
		return nil, err
	}

	return rs, nil
}
