package exchange

import (
	"crypto/ed25519"
	"crypto/sha256"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/firelizzard/rmxp/object"
)

func TestIdentity(t *testing.T) {
	seed := sha256.Sum256([]byte(t.Name()))
	remote := ed25519.NewKeyFromSeed(seed[:])

	depot := &DepotClient{
		Caller: &SignedCaller{
			Inner: &DepotServer{},
			Key:   remote,
		},
	}

	expect := sha256.Sum256(remote[32:])
	require.Equal(t, object.ID(expect), depot.ID())
}
