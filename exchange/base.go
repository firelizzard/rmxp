package exchange

import (
	"crypto"

	"gitlab.com/firelizzard/rmxp/message"
	"gitlab.com/firelizzard/rmxp/object"
	"gitlab.com/firelizzard/rmxp/smp"
)

type mergeBaseRequest struct {
	base  object.Base
	Ref   string
	Since object.ID
	Have  []object.ID
}

type mergeBaseResponse struct {
	base object.Base
	Base object.ID
}

func (r *mergeBaseRequest) Wrap(s message.MergeBaseArgs) {
	r.Ref = s.Ref
	r.Since = s.Since
	r.Have = s.Have
}

func (r *mergeBaseRequest) Unwrap() message.MergeBaseArgs {
	return message.MergeBaseArgs{
		Ref:   r.Ref,
		Since: r.Since,
		Have:  r.Have,
	}
}

func (r *mergeBaseResponse) Wrap(v object.ID)  { r.Base = v }
func (r *mergeBaseResponse) Unwrap() object.ID { return r.Base }

var typeMergeBaseRequest = object.RegisterType[*mergeBaseRequest]("mergeBaseRq")
var typeMergeBaseResponse = object.RegisterType[*mergeBaseResponse]("mergeBaseRs")

func (e *mergeBaseRequest) ID() object.ID  { return e.base.ID(e) }
func (e *mergeBaseResponse) ID() object.ID { return e.base.ID(e) }

func (e *mergeBaseRequest) Sender() (object.ID, bool)  { return e.base.Sender() }
func (e *mergeBaseResponse) Sender() (object.ID, bool) { return e.base.Sender() }

func (e *mergeBaseRequest) Sign(key crypto.PrivateKey) error  { return e.base.Sign(e, key) }
func (e *mergeBaseResponse) Sign(key crypto.PrivateKey) error { return e.base.Sign(e, key) }

func (e *mergeBaseRequest) MarshalSMP() (*smp.Message, error) {
	type E mergeBaseRequest
	return e.base.Marshal((*E)(e), typeMergeBaseRequest)
}

func (e *mergeBaseRequest) UnmarshalSMP(m *smp.Message) error {
	*e = mergeBaseRequest{}
	type E mergeBaseRequest
	return e.base.Unmarshal(m, (*E)(e), typeMergeBaseRequest)
}

func (e *mergeBaseResponse) MarshalSMP() (*smp.Message, error) {
	type E mergeBaseResponse
	return e.base.Marshal((*E)(e), typeMergeBaseResponse)
}

func (e *mergeBaseResponse) UnmarshalSMP(m *smp.Message) error {
	*e = mergeBaseResponse{}
	type E mergeBaseResponse
	return e.base.Unmarshal(m, (*E)(e), typeMergeBaseResponse)
}
