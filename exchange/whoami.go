package exchange

import (
	"crypto"

	"gitlab.com/firelizzard/rmxp/object"
	"gitlab.com/firelizzard/rmxp/smp"
)

type whoamiRequest struct {
	base object.Base
}

type whoamiResponse struct {
	base object.Base
}

var typeWhoamiRequest = "whoamiRq"
var typeWhoamiResponse = "whoamiRs"

func init() {
	object.RegisterType[*whoamiRequest](typeWhoamiRequest)
	object.RegisterType[*whoamiResponse](typeWhoamiResponse)
}

func (e *whoamiRequest) ID() object.ID              { return e.base.ID(e) }
func (e *whoamiRequest) Sender() (object.ID, bool)  { return e.base.Sender() }
func (e *whoamiResponse) ID() object.ID             { return e.base.ID(e) }
func (e *whoamiResponse) Sender() (object.ID, bool) { return e.base.Sender() }

func (e *whoamiRequest) Sign(key crypto.PrivateKey) error  { return e.base.Sign(e, key) }
func (e *whoamiResponse) Sign(key crypto.PrivateKey) error { return e.base.Sign(e, key) }

func (e *whoamiRequest) MarshalSMP() (*smp.Message, error) {
	type E whoamiRequest
	return e.base.Marshal((*E)(e), typeWhoamiRequest)
}

func (e *whoamiRequest) UnmarshalSMP(m *smp.Message) error {
	*e = whoamiRequest{}
	type E whoamiRequest
	return e.base.Unmarshal(m, (*E)(e), typeWhoamiRequest)
}

func (e *whoamiResponse) MarshalSMP() (*smp.Message, error) {
	type E whoamiResponse
	return e.base.Marshal((*E)(e), typeWhoamiResponse)
}

func (e *whoamiResponse) UnmarshalSMP(m *smp.Message) error {
	*e = whoamiResponse{}
	type E whoamiResponse
	return e.base.Unmarshal(m, (*E)(e), typeWhoamiResponse)
}
