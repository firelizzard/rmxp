package exchange

import (
	"context"
	"log/slog"

	"gitlab.com/firelizzard/rmxp/message"
	"gitlab.com/firelizzard/rmxp/object"
)

type DepotServer struct {
	Depot message.Depot
}

type DepotClient struct {
	Caller Caller

	id object.ID
}

type Caller interface {
	Call(context.Context, object.Object) (object.Object, error)
}

var _ message.Depot = (*DepotClient)(nil)

func (s *DepotServer) Call(ctx context.Context, rq object.Object) (object.Object, error) {
	switch rq := rq.(type) {
	case *mergeBaseRequest:
		return serverCall[*mergeBaseResponse](ctx, s.Depot.MergeBase, rq), nil
	case *pullRequest:
		return serverCall[*pullResponse](ctx, s.Depot.Pull, rq), nil
	case *pushRequest:
		return serverCall[*pushResponse](ctx, s.Depot.Push, rq), nil
	case *sinceRequest:
		return serverCall[*sinceResponse](ctx, s.Depot.Since, rq), nil
	case *whoamiRequest:
		return &whoamiResponse{}, nil
	}
	return nil, object.ErrInvalid.WithFormat("%T is not a supported request type", rq)
}

func (c *DepotClient) ID() object.ID {
	if (c.id != object.ID{}) {
		return c.id
	}

	rs, err := c.Caller.Call(context.Background(), &whoamiRequest{})
	if err != nil {
		slog.Error("Failed to determine ID", "error", err)
		return object.ID{}
	}

	id, ok := rs.Sender()
	if !ok {
		return object.ID{}
	}

	c.id = id
	return id
}

func (c *DepotClient) MergeBase(ctx context.Context, args message.MergeBaseArgs) (object.ID, error) {
	return clientCall[*mergeBaseResponse, *mergeBaseRequest](c, ctx, args)
}

func (c *DepotClient) Pull(ctx context.Context, args message.PullArgs) ([]*message.Message, error) {
	return clientCall[*pullResponse, *pullRequest](c, ctx, args)
}

func (c *DepotClient) Push(ctx context.Context, args message.PushArgs) (newHead *message.Message, err error) {
	return clientCall[*pushResponse, *pushRequest](c, ctx, args)
}

func (c *DepotClient) Since(ctx context.Context, args message.SinceArgs) ([]object.ID, error) {
	return clientCall[*sinceResponse, *sinceRequest](c, ctx, args)
}

func serverCall[PT interface {
	~*T
	Wrap(U)
	object.Object
}, V, U, T any](ctx context.Context, method func(context.Context, V) (U, error), rq interface{ Unwrap() V }) object.Object {
	v, err := method(ctx, rq.Unwrap())
	if err == nil {
		rs := PT(new(T))
		rs.Wrap(v)
		return rs
	}
	if err, ok := err.(*object.Error); ok {
		return err
	}
	return object.ErrUnknown.With(err)
}

func clientCall[RS interface{ Unwrap() U }, RQ interface {
	~*T
	Wrap(V)
	object.Object
}, V, U, T any](c *DepotClient, ctx context.Context, v V) (U, error) {
	rq := RQ(new(T))
	rq.Wrap(v)
	rs, err := c.Caller.Call(ctx, rq)
	var z U
	if err != nil {
		return z, err
	}
	switch rs := rs.(type) {
	case *object.Error:
		return z, rs
	case RS:
		return rs.Unwrap(), nil
	default:
		return z, object.ErrInvalid.WithFormat("invalid response: want %T, got %T", z, rs)
	}
}
