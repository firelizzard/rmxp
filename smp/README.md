# Simple Message Protocol

SMP is a simple protocol for exchanging messages, based on HTTP and HTTP-like protocols such as [Microsoft's LSP][ms-lsp]

[ms-lsp]: https://microsoft.github.io/language-server-protocol

```
$signed
```