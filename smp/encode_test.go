package smp

import (
	"bytes"
	"encoding"
	"fmt"
	"strconv"
	"testing"
)

type MyMessage struct {
	Foo    []string
	Bar    *Byte             `smp:"bar"`
	Fields map[string]string `smp:"-,fields"`
	Body   string            `smp:"-,body"`
}

type Byte [1]byte

var _ encoding.TextMarshaler = Byte{}
var _ encoding.TextUnmarshaler = &Byte{}

func (b Byte) MarshalText() ([]byte, error) {
	return []byte(fmt.Sprint(int(b[0]))), nil
}

func (b *Byte) UnmarshalText(c []byte) error {
	v, err := strconv.ParseInt(string(c), 10, 8)
	if err != nil {
		return err
	}
	(*b)[0] = byte(v)
	return nil
}

func TestEncoder_Write(t *testing.T) {
	buf := new(bytes.Buffer)
	e := &Encoder{w: buf}
	err := e.Write(&Message{
		Fields: Fields{
			{"Foo", "Bar"},
			{"Baz", "Bat"},
		},
		Body: []byte("Hello, world"),
	})
	assert(t, err == nil)
	assert(t, buf.String() == ""+
		"Foo: Bar\r\n"+
		"Baz: Bat\r\n"+
		"Content-Length: 12\r\n"+
		"\r\n"+
		"Hello, world")
}

func TestEncodeStruct(t *testing.T) {
	buf := new(bytes.Buffer)
	e := &Encoder{w: buf}
	bar := Byte{2}
	err := e.Encode(MyMessage{
		Foo: []string{"1", "10"},
		Bar: &bar,
		Fields: map[string]string{
			"baz": "3",
		},
		Body: "Hello, world",
	})
	assert(t, err == nil)
	assert(t, buf.String() == ""+
		"Foo: 1\r\n"+
		"Foo: 10\r\n"+
		"bar: 2\r\n"+
		"baz: 3\r\n"+
		"Content-Length: 12\r\n"+
		"\r\n"+
		"Hello, world")
}
