package smp

import (
	"crypto"
	"crypto/ed25519"
	"crypto/sha256"
	"encoding"
	"errors"
	"fmt"
	"reflect"
	"strings"
	"sync"
)

var (
	tMarshaller       = reflect.TypeOf(new(Marshaller)).Elem()
	tUnmarshaller     = reflect.TypeOf(new(Unmarshaller)).Elem()
	tTextMarshaller   = reflect.TypeOf(new(encoding.TextMarshaler)).Elem()
	tTextUnmarshaller = reflect.TypeOf(new(encoding.TextUnmarshaler)).Elem()
	tFieldSlice       = reflect.TypeOf([]Field{})
	tFieldMap         = reflect.TypeOf(map[string]string{})
	tBytes            = reflect.TypeOf([]byte{})
	tString           = reflect.TypeOf("")

	messageEncoderCache = new(sync.Map)
	messageDecoderCache = new(sync.Map)
	valueEncoderCache   = new(sync.Map)
	valueDecoderCache   = new(sync.Map)
)

type Marshaller interface {
	MarshalSMP() (*Message, error)
}

type Unmarshaller interface {
	UnmarshalSMP(*Message) error
}

type messageEncoder func(reflect.Value) (*Message, error)
type messagePartEncoder func(reflect.Value, *Message) error
type messageDecoder func(reflect.Value, *Message) error
type messagePartDecoder func(reflect.Value, *Message) error
type valueEncoder func(reflect.Value) ([]byte, error)
type valueDecoder func(reflect.Value, []byte) error
type sliceEncoder func(reflect.Value) ([][]byte, error)

type Message struct {
	Fields    Fields
	Body      []byte
	Signature *Signature
}

type Signature struct {
	Type      string
	PublicKey []byte
	Signature []byte
}

type Fields []Field

type Field struct {
	Name  string
	Value string
}

func (m *Message) Copy() *Message {
	n := new(Message)
	n.Fields = make(Fields, len(m.Fields))
	copy(n.Fields, m.Fields)
	n.Body = make([]byte, len(m.Body))
	copy(n.Body, m.Body)
	return n
}

func (f Fields) Get(name string) (string, bool) {
	for _, f := range f {
		if strings.EqualFold(f.Name, name) {
			return f.Value, true
		}
	}
	return "", false
}

func (f Fields) GetAll(name string) []string {
	var values []string
	for _, f := range f {
		if strings.EqualFold(f.Name, name) {
			values = append(values, f.Value)
		}
	}
	return values
}

func (f *Fields) Add(name, value string) {
	*f = append(*f, Field{Name: name, Value: value})
}

// Hash calculates the SHA-256 hash of the encoded message. Hash excludes the
// signature.
func (m *Message) Hash() ([]byte, error) {
	h := sha256.New()
	enc := NewEncoder(h)
	enc.SkipSignature = true
	err := enc.Write(m)
	if err != nil {
		return nil, err
	}
	return h.Sum(nil), nil
}

func (m *Message) Sign(key crypto.PrivateKey) error {
	hash, err := m.Hash()
	if err != nil {
		return err
	}

	m.Signature = new(Signature)
	switch key := key.(type) {
	case ed25519.PrivateKey:
		m.Signature.Type = "ed25519"
		m.Signature.PublicKey = key.Public().(ed25519.PublicKey)
		m.Signature.Signature = ed25519.Sign(key, hash)
	default:
		return errors.New(fmt.Sprintf("unsupported key type: %T", key))
	}
	return nil
}

func (s *Signature) Verify(m *Message) error {
	hash, err := m.Hash()
	if err != nil {
		return err
	}

	var ok bool
	switch s.Type {
	case "ed25519":
		ok = ed25519.Verify(s.PublicKey, hash, s.Signature)
	default:
		return errors.New(fmt.Sprintf("unknown signature type %q", s.Type))
	}
	if !ok {
		return errors.New("invalid signature")
	}
	return nil
}
