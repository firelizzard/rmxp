package smp

import (
	"bytes"
	"errors"
	"io"
	"testing"
)

func TestDecoder_Read(t *testing.T) {
	rd := bytes.NewReader([]byte("" +
		"Foo: Bar\r\n" +
		"Baz: Bat\r\n" +
		"Content-Length: 12\r\n" +
		"\r\n" +
		"Hello, world" +
		"Content-Length: 0\r\n" +
		"\r\n"))

	d := &Decoder{r: rd}
	m, err := d.Read()
	assert(t, err == nil)
	assert(t, len(m.Fields) == 3)
	assert(t, m.Fields[0].Name == "Foo")
	assert(t, m.Fields[0].Value == "Bar")
	assert(t, m.Fields[1].Name == "Baz")
	assert(t, m.Fields[1].Value == "Bat")
	assert(t, string(m.Body) == "Hello, world")

	m, err = d.Read()
	assert(t, err == nil)
	assert(t, len(m.Fields) == 1)
	assert(t, string(m.Body) == "")

	_, err = d.Read()
	assert(t, errors.Is(err, io.EOF))
}

func TestDecodeStruct(t *testing.T) {
	rd := bytes.NewReader([]byte("" +
		"Foo: 1\r\n" +
		"Foo: 10\r\n" +
		"bar: 2\r\n" +
		"baz: 3\r\n" +
		"Content-Length: 12\r\n" +
		"\r\n" +
		"Hello, world"))

	d := &Decoder{r: rd}
	var m MyMessage
	err := d.Decode(&m)
	assert(t, err == nil)
	assert(t, len(m.Foo) == 2)
	assert(t, m.Foo[0] == "1")
	assert(t, m.Foo[1] == "10")
	assert(t, m.Bar != nil && *m.Bar == [1]byte{2})
	assert(t, len(m.Fields) == 1)
	assert(t, m.Fields["baz"] == "3")
	assert(t, string(m.Body) == "Hello, world")
}

func assert(t testing.TB, ok bool) {
	t.Helper()
	if !ok {
		t.Fatalf("Assertion failed")
	}
}
