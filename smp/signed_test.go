package smp

import (
	"bytes"
	"crypto/ed25519"
	"crypto/sha256"
	"encoding/hex"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestSigning(t *testing.T) {
	seed := sha256.Sum256([]byte(t.Name()))
	key := ed25519.NewKeyFromSeed(seed[:])

	msg := MyMessage{
		Foo: []string{"1", "10"},
		Fields: map[string]string{
			"baz": "3",
		},
		Body: "Hello, world",
	}

	buf := new(bytes.Buffer)
	enc := NewEncoder(buf)
	enc.SignWith = key
	require.NoError(t, enc.Encode(msg))

	dec := NewDecoder(buf)
	dec.SkipSignatureVerification = true
	out, err := dec.Read()
	require.NoError(t, err)
	require.NotNil(t, out.Signature)
	require.Equal(t, hex.EncodeToString(key[32:]), hex.EncodeToString(out.Signature.PublicKey))
	require.NoError(t, out.Signature.Verify(out))
}
