package smp

import (
	"bytes"
	"crypto"
	"encoding"
	"errors"
	"fmt"
	"io"
	"reflect"
	"strings"
	"unicode"
)

func Marshal(v any) ([]byte, error) {
	buf := new(bytes.Buffer)
	err := (&Encoder{w: buf}).Encode(v)
	return buf.Bytes(), err
}

type Encoder struct {
	w             io.Writer
	SkipSignature bool
	SignWith      crypto.PrivateKey
}

func NewEncoder(w io.Writer) *Encoder {
	return &Encoder{w: w}
}

func (e *Encoder) Encode(v any) error {
	m, err := e.Marshal(v)
	if err != nil {
		return err
	}

	return e.Write(m)
}

func (e *Encoder) Marshal(v any) (*Message, error) {
	if v == nil {
		return nil, errors.New("value is nil")
	}

	var m *Message
	switch v := v.(type) {
	case Marshaller:
		var err error
		m, err = v.MarshalSMP()
		if err != nil {
			return nil, err
		}

		m = m.Copy()

	case Message:
		m = v.Copy()

	case *Message:
		m = v.Copy()

	default:
		rv := reflect.ValueOf(v)
		fn, err := messageEncoderFor(rv.Type())
		if err != nil {
			return nil, err
		}

		m, err = fn(rv)
		if err != nil {
			return nil, err
		}
	}

	if e.SignWith == nil {
		return m, nil
	}

	return m, m.Sign(e.SignWith)
}

func messageEncoderFor(typ reflect.Type) (messageEncoder, error) {
	if fn, ok := messageEncoderCache.Load(typ); ok {
		return fn.(messageEncoder), nil
	}

	// *v is a Marshaller?
	isPtr := typ.Kind() == reflect.Ptr
	if isPtr && typ.Elem().AssignableTo(tMarshaller) {
		fn := func(v reflect.Value) (*Message, error) {
			u := v.Elem().Interface().(Marshaller)
			return u.MarshalSMP()
		}
		messageEncoderCache.Store(typ, fn)
		return fn, nil
	}

	// &v is a Marshaller?
	if isPtr && reflect.PointerTo(typ).AssignableTo(tMarshaller) {
		fn := func(v reflect.Value) (*Message, error) {
			if !v.CanAddr() {
				return nil, errors.New("cannot take the address of the value")
			}
			u := v.Addr().Interface().(Marshaller)
			return u.MarshalSMP()
		}
		messageEncoderCache.Store(typ, fn)
		return fn, nil
	}

	if isPtr {
		typ = typ.Elem()
	}
	if typ.Kind() != reflect.Struct {
		return nil, fmt.Errorf("cannot encode %v", typ)
	}

	var parts []messagePartEncoder
	for i, n := 0, typ.NumField(); i < n; i++ {
		f := typ.Field(i)
		if !f.IsExported() {
			continue
		}

		i := i
		get := func(v reflect.Value) reflect.Value {
			return v.Field(i)
		}
		if isPtr {
			g := get
			get = func(v reflect.Value) reflect.Value {
				return g(v.Elem())
			}
		}
		typ := f.Type
		if typ.Kind() == reflect.Ptr {
			typ = typ.Elem()
			g := get
			get = func(v reflect.Value) reflect.Value {
				return g(v).Elem()
			}
		}

		name := f.Name
		tag := strings.Split(f.Tag.Get("smp"), ",")
		switch {
		case tag[0] == "-" && len(tag) > 1 && tag[1] == "body":
			venc, err := valueEncoderFor(typ)
			if err != nil {
				return nil, fmt.Errorf("field %s: %w", f.Name, err)
			}
			parts = append(parts, func(v reflect.Value, m *Message) error {
				s, err := venc(get(v))
				if err != nil {
					return fmt.Errorf("field %s: %w", f.Name, err)
				}
				m.Body = s
				return nil
			})
			continue

		case tag[0] == "-" && len(tag) > 1 && tag[1] == "fields":
			switch {
			case typ.AssignableTo(tFieldSlice):
				parts = append(parts, func(v reflect.Value, m *Message) error {
					for _, f := range getAs[[]Field](get(v)) {
						m.Fields = append(m.Fields, f)
					}
					return nil
				})

			case typ.AssignableTo(tFieldMap):
				parts = append(parts, func(v reflect.Value, m *Message) error {
					for name, value := range getAs[map[string]string](get(v)) {
						m.Fields.Add(name, value)
					}
					return nil
				})

			default:
				return nil, fmt.Errorf("%v is not a valid message fields type", typ)
			}
			continue

		case tag[0] == "-":
			continue

		case tag[0] != "":
			name = tag[0]
		}

		repeat := isRepeatable(typ)
		if repeat {
			typ = typ.Elem()
		}

		venc, err := valueEncoderFor(typ)
		if err != nil {
			return nil, fmt.Errorf("field %s: %w", f.Name, err)
		}

		parts = append(parts, func(v reflect.Value, m *Message) error {
			v = get(v)
			if !v.IsValid() || v.IsZero() {
				return nil
			}

			if !repeat {
				b, err := venc(v)
				if err != nil {
					return fmt.Errorf("field %s: %w", f.Name, err)
				}
				m.Fields.Add(name, string(b))
				return nil
			}

			for i, n := 0, v.Len(); i < n; i++ {
				b, err := venc(v.Index(i))
				if err != nil {
					return err
				}
				m.Fields.Add(name, string(b))
			}
			return nil
		})
	}

	fn := func(v reflect.Value) (*Message, error) {
		var m Message
		for _, fn := range parts {
			fn(v, &m)
		}
		return &m, nil
	}
	messageEncoderCache.Store(typ, fn)
	return fn, nil
}

func isRepeatable(typ reflect.Type) bool {
	return typ.Kind() == reflect.Slice &&
		!typ.AssignableTo(tTextMarshaller) &&
		!typ.AssignableTo(tBytes) &&
		!typ.AssignableTo(tString)
}

func valueEncoderFor(typ reflect.Type) (valueEncoder, error) {
	if fn, ok := valueEncoderCache.Load(typ); ok {
		return fn.(valueEncoder), nil
	}

	var fn valueEncoder
	switch {
	case typ.AssignableTo(tTextMarshaller):
		fn = func(v reflect.Value) ([]byte, error) {
			b, err := getAs[encoding.TextMarshaler](v).MarshalText()
			return b, err
		}
		goto done

	case typ.AssignableTo(tBytes):
		fn = func(v reflect.Value) ([]byte, error) {
			return getAs[[]byte](v), nil
		}
		goto done

	case typ.AssignableTo(tString):
		fn = func(v reflect.Value) ([]byte, error) {
			return []byte(getAs[string](v)), nil
		}
		goto done
	}

	switch typ.Kind() {
	case reflect.Bool,
		reflect.Int,
		reflect.Int8,
		reflect.Int16,
		reflect.Int32,
		reflect.Int64,
		reflect.Uint,
		reflect.Uint8,
		reflect.Uint16,
		reflect.Uint32,
		reflect.Uint64,
		reflect.Float32,
		reflect.Float64:
		fn = func(v reflect.Value) ([]byte, error) {
			return []byte(fmt.Sprint(v.Interface())), nil
		}

	default:
		return nil, fmt.Errorf("unable to encode %v", typ)
	}

done:
	valueEncoderCache.Store(typ, fn)
	return fn, nil
}

func getAs[T any](v reflect.Value) T {
	var u T
	reflect.ValueOf(&u).Elem().Set(v)
	return u
}

func (e *Encoder) Write(m *Message) error {
	if m.Signature != nil && !e.SkipSignature {
		if m.Signature.Type == "" || len(m.Signature.PublicKey) == 0 || len(m.Signature.Signature) == 0 {
			return errors.New("invalid signature")
		}

		_, err := e.w.Write([]byte(fmt.Sprintf("$signed %s %x %x\r\n",
			m.Signature.Type,
			m.Signature.PublicKey,
			m.Signature.Signature)))
		if err != nil {
			return err
		}
	}

	var hasCL bool
	for _, f := range m.Fields {
		err := checkFieldStr(f.Name, true)
		if err != nil {
			return err
		}
		err = checkFieldStr(f.Value, false)
		if err != nil {
			return err
		}

		if strings.EqualFold(f.Name, "content-length") {
			hasCL = true
		}

		_, err = e.w.Write([]byte(f.Name + ": " + f.Value + "\r\n"))
		if err != nil {
			return err
		}
	}

	if !hasCL {
		_, err := e.w.Write([]byte(fmt.Sprintf("Content-Length: %d\r\n", len(m.Body))))
		if err != nil {
			return err
		}
	}

	_, err := e.w.Write([]byte("\r\n"))
	if err != nil {
		return err
	}

	_, err = e.w.Write(m.Body)
	return err
}

func checkFieldStr(s string, name bool) error {
	for i, c := range s {
		if i == 0 && name && !unicode.IsLetter(c) {
			return errors.New("field names must start with a letter")
		}

		switch c {
		case '\r', '\n':
			return errors.New("fields must not contain newlines")
		case ':':
			if name {
				return errors.New("field names must not contain colons")
			}
		}
		if c < ' ' || c > '~' {
			return fmt.Errorf("fields must be printable ASCII")
		}
	}

	return nil
}
