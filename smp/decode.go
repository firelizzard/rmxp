package smp

import (
	"bytes"
	"encoding"
	"encoding/hex"
	"errors"
	"fmt"
	"io"
	"reflect"
	"strconv"
	"strings"
)

func Unmarshal(b []byte, v any) error {
	return (&Decoder{buf: b}).Decode(v)
}

type Decoder struct {
	r     io.Reader
	buf   []byte
	i, j  int
	cl    int
	gotCL bool

	// SkipSignatureVerification disables signature verification.
	SkipSignatureVerification bool
}

func NewDecoder(r io.Reader) *Decoder {
	return &Decoder{r: r}
}

func (d *Decoder) Decode(v any) error {
	m, err := d.Read()
	if err != nil {
		return err
	}

	return m.Unmarshal(v)
}

func (m *Message) Unmarshal(v any) error {
	if v == nil {
		return errors.New("cannot decode into nil")
	}

	switch v := v.(type) {
	case Unmarshaller:
		return v.UnmarshalSMP(m)

	case *Message:
		*v = *m.Copy()
		return nil
	}

	rv := reflect.ValueOf(v)
	fn, err := messageDecoderFor(rv.Type())
	if err != nil {
		return err
	}
	return fn(rv, m)
}

func messageDecoderFor(typ reflect.Type) (messageDecoder, error) {
	if fn, ok := messageDecoderCache.Load(typ); ok {
		return fn.(messageDecoder), nil
	}

	// *v is a Marshaller?
	isPtr := typ.Kind() == reflect.Ptr
	if isPtr && typ.Elem().AssignableTo(tUnmarshaller) {
		fn := func(v reflect.Value, m *Message) error {
			u := v.Elem().Interface().(Unmarshaller)
			return u.UnmarshalSMP(m)
		}
		messageDecoderCache.Store(typ, fn)
		return fn, nil
	}

	if !isPtr {
		return nil, fmt.Errorf("cannot decode %v", typ)
	}
	typ = typ.Elem()
	if typ.Kind() != reflect.Struct {
		return nil, fmt.Errorf("cannot decode %v", typ)
	}

	var parts []messagePartDecoder
	explicit := map[string]bool{
		"content-length": true,
	}
	for i, n := 0, typ.NumField(); i < n; i++ {
		f := typ.Field(i)
		if !f.IsExported() {
			continue
		}

		i := i
		get := func(v reflect.Value) reflect.Value {
			return v.Elem().Field(i)
		}
		typ := f.Type
		if typ.Kind() == reflect.Ptr {
			typ = typ.Elem()
			g := get
			get = func(v reflect.Value) reflect.Value {
				v = g(v)
				if v.IsNil() {
					v.Set(reflect.New(typ))
				}
				return v.Elem()
			}
		}

		name := f.Name
		tag := strings.Split(f.Tag.Get("smp"), ",")
		switch {
		case tag[0] == "-" && len(tag) > 1 && tag[1] == "body":
			vdec, err := valueDecoderFor(typ)
			if err != nil {
				return nil, fmt.Errorf("field %s: %w", f.Name, err)
			}
			parts = append(parts, func(v reflect.Value, m *Message) error {
				err := vdec(get(v), m.Body)
				if err != nil {
					return fmt.Errorf("field %s: %w", f.Name, err)
				}
				return nil
			})
			continue

		case tag[0] == "-" && len(tag) > 1 && tag[1] == "fields":
			switch {
			case typ.AssignableTo(tFieldSlice):
				parts = append(parts, func(v reflect.Value, m *Message) error {
					fields := getAs[*[]Field](get(v).Addr())
					for _, f := range m.Fields {
						if !explicit[strings.ToLower(f.Name)] {
							*fields = append(*fields, f)
						}
					}
					return nil
				})

			case typ.AssignableTo(tFieldMap):
				parts = append(parts, func(v reflect.Value, m *Message) error {
					v = get(v)
					if v.IsNil() {
						v.Set(reflect.ValueOf(map[string]string{}))
					}
					fields := getAs[map[string]string](v)
					for _, f := range m.Fields {
						if !explicit[strings.ToLower(f.Name)] {
							fields[f.Name] = f.Value
						}
					}
					return nil
				})

			default:
				return nil, fmt.Errorf("%v is not a valid message fields type", typ)
			}
			continue

		case tag[0] == "-":
			continue

		case tag[0] != "":
			name = tag[0]
		}

		explicit[strings.ToLower(name)] = true

		repeat := isRepeatable(typ)
		if repeat {
			typ = typ.Elem()
		}

		vdec, err := valueDecoderFor(typ)
		if err != nil {
			return nil, fmt.Errorf("field %s: %w", f.Name, err)
		}
		parts = append(parts, func(v reflect.Value, m *Message) error {
			s := m.Fields.GetAll(name)
			if len(s) == 0 {
				return nil
			}

			if !repeat {
				err := vdec(get(v), []byte(s[0]))
				if err != nil {
					return fmt.Errorf("field %s: %w", f.Name, err)
				}
				return nil
			}

			v = get(v)
			u := reflect.MakeSlice(v.Type(), len(s), len(s))
			for i, s := range s {
				err := vdec(u.Index(i), []byte(s))
				if err != nil {
					return fmt.Errorf("field %s: %w", f.Name, err)
				}
			}
			v.Set(u)
			return nil
		})
	}

	fn := func(v reflect.Value, m *Message) error {
		for _, fn := range parts {
			fn(v, m)
		}
		return nil
	}
	messageDecoderCache.Store(typ, fn)
	return fn, nil
}

func valueDecoderFor(typ reflect.Type) (valueDecoder, error) {
	if fn, ok := valueDecoderCache.Load(typ); ok {
		return fn.(valueDecoder), nil
	}

	var fn valueDecoder
	var parse func(string) (reflect.Value, error)
	switch {
	case typ.AssignableTo(tTextUnmarshaller):
		fn = func(v reflect.Value, b []byte) error {
			return getAs[encoding.TextUnmarshaler](v).UnmarshalText(b)
		}
		goto done

	case reflect.PointerTo(typ).AssignableTo(tTextUnmarshaller):
		fn = func(v reflect.Value, b []byte) error {
			return getAs[encoding.TextUnmarshaler](v.Addr()).UnmarshalText(b)
		}
		goto done

	case typ.AssignableTo(tBytes):
		fn = func(v reflect.Value, b []byte) error {
			v.Set(reflect.ValueOf(b))
			return nil
		}
		goto done

	case typ.AssignableTo(tString):
		fn = func(v reflect.Value, b []byte) error {
			v.Set(reflect.ValueOf(string(b)))
			return nil
		}
		goto done
	}

	switch typ.Kind() {
	case reflect.Bool:
		parse = func(s string) (reflect.Value, error) {
			v, err := strconv.ParseBool(s)
			if err != nil {
				return reflect.Value{}, err
			}
			return reflect.ValueOf(v), nil
		}

	case reflect.Int,
		reflect.Int8,
		reflect.Int16,
		reflect.Int32,
		reflect.Int64:
		parse = func(s string) (reflect.Value, error) {
			v, err := strconv.ParseInt(s, 10, 64)
			if err != nil {
				return reflect.Value{}, err
			}
			return reflect.ValueOf(v), nil
		}

	case reflect.Uint,
		reflect.Uint8,
		reflect.Uint16,
		reflect.Uint32,
		reflect.Uint64:
		parse = func(s string) (reflect.Value, error) {
			v, err := strconv.ParseUint(s, 10, 64)
			if err != nil {
				return reflect.Value{}, err
			}
			return reflect.ValueOf(v), nil
		}

	case reflect.Float32,
		reflect.Float64:
		parse = func(s string) (reflect.Value, error) {
			v, err := strconv.ParseFloat(s, 64)
			if err != nil {
				return reflect.Value{}, err
			}
			return reflect.ValueOf(v), nil
		}

	default:
		return nil, fmt.Errorf("unable to decode %v", typ)
	}
	fn = func(v reflect.Value, b []byte) error {
		u, err := parse(string(b))
		if err != nil {
			return err
		}
		v.Set(u.Convert(typ))
		return nil
	}

done:
	valueDecoderCache.Store(typ, fn)
	return fn, nil
}

func (d *Decoder) Read() (*Message, error) {
	var msg Message

	// Read fields
	var first = true
	for !d.gotCL {
		// Read the next line
		s, err := d.next()
		if err != nil {
			if !first && errors.Is(err, io.EOF) {
				return nil, io.ErrUnexpectedEOF
			}
			return nil, err
		}
		first = false
		if len(s) == 0 {
			break
		}

		// Is it a signature?
		if bytes.HasPrefix(s, []byte("$signed ")) {
			s := bytes.Split(s, []byte(" "))
			if len(s) < 4 {
				return nil, fmt.Errorf("invalid signature: want 4 parts, got %d", len(s))
			}
			msg.Signature = new(Signature)
			msg.Signature.Type = string(s[1])
			msg.Signature.PublicKey = unhex(&err, s[2])
			msg.Signature.Signature = unhex(&err, s[3])
			if err != nil {
				return nil, fmt.Errorf("invalid signature: %w", err)
			}
			continue
		}

		// Parse the field
		i := bytes.Index(s, []byte(": "))
		var name, value string
		if i < 0 {
			name = string(s)
		} else {
			name, value = string(s[:i]), string(s[i+2:])
		}
		msg.Fields.Add(name, value)

		// Parse the content length
		if strings.EqualFold(name, "content-length") {
			v, err := strconv.ParseUint(value, 10, 31)
			if err != nil {
				return nil, fmt.Errorf("invalid content length: %w", err)
			}
			d.cl = int(v)
			d.gotCL = true
		}
	}

	if !d.gotCL {
		return nil, errors.New("invalid message: missing content length")
	}

	// Read the body and reset
	msg.Body = make([]byte, d.cl)
	n := copy(msg.Body, d.buf[d.i+2:d.j])
	d.i += n + 2
	d.gotCL = false
	_, err := io.ReadFull(d.r, msg.Body[n:])
	if err != nil {
		return nil, err
	}

	if msg.Signature != nil && !d.SkipSignatureVerification {
		err := msg.Signature.Verify(&msg)
		if err != nil {
			return nil, err
		}
	}

	return &msg, nil
}

func (d *Decoder) next() ([]byte, error) {
	for {
		i := bytes.Index(d.buf[d.i:d.j], []byte("\r\n"))
		if i >= 0 {
			s := d.buf[d.i : d.i+i]
			d.i += i + 2
			return s, nil
		}

		err := d.more()
		if err != nil {
			return nil, err
		}
	}
}

func (d *Decoder) more() error {
	if d.r == nil {
		return io.EOF
	}

	// Allocate the read buffer
	if d.buf == nil {
		d.buf = make([]byte, 512)
	}

	// Move data to the beginning
	if d.i > 0 {
		copy(d.buf, d.buf[d.i:d.j])
		d.j -= d.i
		d.i = 0
	}

	// Allocate more space?
	if d.j >= len(d.buf) {
		b := make([]byte, len(d.buf)+512)
		copy(b, d.buf)
		d.buf = b
	}

	// Read
	n, err := d.r.Read(d.buf[d.j:])
	if err != nil {
		return err
	}
	if n == 0 {
		return errors.New("read zero bytes")
	}

	d.j += n
	return nil
}

func unhex(err *error, b []byte) []byte {
	if *err != nil {
		return nil
	}

	n := len(b) / 2
	if n*2 < len(b) {
		n++
	}
	c := make([]byte, n)
	n, *err = hex.Decode(c, b)
	return c[:n]
}
