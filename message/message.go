package message

import (
	"crypto"
	"time"

	"gitlab.com/firelizzard/rmxp/object"

	"gitlab.com/firelizzard/rmxp/smp"
)

type Message struct {
	base      object.Base
	Parent    []object.ID
	Author    string
	Timestamp time.Time
	ReplyTo   *object.ID
	Content   string `smp:"-,body"`
}

var typeMessage = "message"

func init() { object.RegisterType[*Message](typeMessage) }

func (m *Message) ID() object.ID                    { return m.base.ID(m) }
func (m *Message) Sender() (object.ID, bool)        { return m.base.Sender() }
func (m *Message) Sign(key crypto.PrivateKey) error { return m.base.Sign(m, key) }

func (m *Message) MarshalSMP() (*smp.Message, error) {
	type M Message
	return m.base.Marshal((*M)(m), typeMessage)
}

func (m *Message) UnmarshalSMP(n *smp.Message) error {
	*m = Message{}
	type M Message
	return m.base.Unmarshal(n, (*M)(m), typeMessage)
}
