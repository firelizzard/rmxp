package message

import (
	"errors"

	"gitlab.com/firelizzard/rmxp/object"
)

func loadThread(store object.Store, ids ...object.ID) ([]*Message, error) {
	var thread []*Message
	for len(ids) > 0 {
		var next []object.ID
		for _, id := range ids {
			msg, err := object.LoadAs[*Message](store, id[:])
			if err != nil {
				// TODO Better error
				return nil, err
			}
			thread = append(thread, msg)
			next = append(next, msg.Parent...)
		}
		ids, next = next, ids[:0]
	}
	return thread, nil
}

func loadRefThread(store object.Store, refName string, allowMissing bool) (*Ref, []*Message, error) {
	ref, err := object.LoadStateAs[*Ref](store, refName)
	switch {
	case errors.Is(err, object.ErrNotFound) && allowMissing:
		return nil, nil, nil
	case err != nil:
		return nil, nil, err
	case ref.Head == object.ID{}:
		return ref, nil, nil
	}

	thread, err := loadThread(store, ref.Head)
	if err != nil {
		return nil, nil, err
	}

	return ref, thread, nil
}

func loadRefGraph(store object.Store, refName string, allowMissing bool) (*Ref, []*Message, *graph, error) {
	ref, thread, err := loadRefThread(store, refName, allowMissing)
	if err != nil {
		return nil, nil, nil, err
	}

	graph := newGraph()
	for _, msg := range thread {
		graph.add(msg)
	}

	return ref, thread, graph, nil
}
