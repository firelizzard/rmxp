package message

import (
	"context"

	"gitlab.com/firelizzard/rmxp/object"
)

type MergeBaseArgs struct {
	Ref   string
	Since object.ID
	Have  []object.ID
}

func GetMergeBase(ctx context.Context, store object.Store, remote Depot, args MergeBaseArgs) (object.ID, error) {
	if args.Have == nil {
		_, thread, err := loadRefThread(store, args.Ref, false)
		if err != nil {
			return object.ID{}, err
		}

		for _, msg := range thread {
			args.Have = append(args.Have, msg.ID())
		}
	}

	return remote.MergeBase(ctx, args)
}

func (d *LocalDepot) MergeBase(ctx context.Context, args MergeBaseArgs) (object.ID, error) {
	_, _, graph, err := loadRefGraph(d.store, args.Ref, true)
	if err != nil {
		return object.ID{}, err
	}

	haveMap := map[object.ID]bool{}
	for _, id := range args.Have {
		haveMap[id] = true
	}

	return graph.base(args.Since, haveMap)
}

func (g *graph) base(base object.ID, have map[object.ID]bool) (object.ID, error) {
	if (base != object.ID{}) && !g.seen[base] {
		return object.ID{}, object.ErrNotFound.WithFormat("message %s not found", base)
	}

	if len(g.roots) == 0 {
		return object.ID{}, nil
	}

	if len(g.roots) != 1 {
		return object.ID{}, object.ErrInvalid.With("multiple roots")
	}

	if (base == object.ID{}) {
		for base = range g.roots {
		}
		if !have[base] {
			return object.ID{}, nil
		}
	}

	ids := []object.ID{base}
	next := map[object.ID]bool{}
	for len(ids) > 0 {
		for _, id := range ids {
			for _, id := range g.children[id] {
				if !have[id] {
					return base, nil
				}
				next[id] = true
			}
		}

		switch len(next) {
		case 0:
			return base, nil
		case 1:
			for base = range next {
			}
		}

		ids = ids[:0]
		for id := range next {
			ids = append(ids, id)
		}
		clear(next)
	}
	return base, nil
}
