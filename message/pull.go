package message

import (
	"context"

	"gitlab.com/firelizzard/rmxp/object"
)

type PullArgs struct {
	Message []object.ID
}

func Pull(ctx context.Context, store object.Store, remote Depot, args PullArgs) error {
	objs, err := remote.Pull(ctx, args)
	if err != nil {
		return err
	}
	for _, obj := range objs {
		err = store.Store(obj)
		if err != nil {
			return err
		}
	}
	return nil
}

func (d *LocalDepot) Pull(ctx context.Context, args PullArgs) ([]*Message, error) {
	var results []*Message
	for _, id := range args.Message {
		msg, err := object.LoadAs[*Message](d.store, id[:])
		if err != nil {
			return nil, err
		}
		results = append(results, msg)
	}
	return results, nil
}
