package message

import (
	"context"

	"gitlab.com/firelizzard/rmxp/object"
)

type Depot interface {
	ID() object.ID
	MergeBase(context.Context, MergeBaseArgs) (object.ID, error)
	Pull(context.Context, PullArgs) ([]*Message, error)
	Push(context.Context, PushArgs) (newHead *Message, err error)
	Since(context.Context, SinceArgs) ([]object.ID, error)
}

type LocalDepot struct {
	id    object.ID
	store object.Store
}

func NewLocalDepot(id object.ID, store object.Store) *LocalDepot {
	return &LocalDepot{id, store}
}

var _ Depot = (*LocalDepot)(nil)

func (d *LocalDepot) ID() object.ID { return d.id }
