package message

import (
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/firelizzard/rmxp/object"
)

func TestGraph(t *testing.T) {
	g := newGraph()
	// parents  map[ID][]ID
	// children map[ID][]ID
	// roots    map[ID]bool
	// leaves   map[ID]bool
	// seen     map[ID]bool
	// orphans  map[ID]bool

	B, A := object.ID{2}, object.ID{1}
	g.addEdge(B, A)
	require.Contains(t, g.parents[B], A)
	require.Contains(t, g.children[A], B)
	require.Empty(t, g.roots)
	require.Contains(t, g.leaves, B)
	require.Contains(t, g.seen, B)
	require.Contains(t, g.orphans, A)

	g.addRoot(A)
	require.Contains(t, g.roots, A)
	require.Contains(t, g.seen, A)
	require.Empty(t, g.orphans)

	D, C := object.ID{4}, object.ID{3}
	g.addEdge(D, C)
	require.Contains(t, g.leaves, D)
	require.Contains(t, g.orphans, C)

	g.addEdge(C, A)
	require.Empty(t, g.orphans)
	require.Len(t, g.leaves, 2)
	require.Contains(t, g.leaves, B)
	require.Contains(t, g.leaves, D)
}
