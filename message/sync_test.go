package message

import (
	"context"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/firelizzard/rmxp/object"
)

func TestSync(t *testing.T) {
	local := new(object.Memstore)
	remote := new(object.Memstore)

	foo := &Message{Content: "foo"}
	bar := &Message{Content: "bar", Parent: []object.ID{foo.ID()}}
	baz := &Message{Content: "baz", Parent: []object.ID{foo.ID()}}

	require.NoError(t, local.Store(foo))
	require.NoError(t, local.Store(bar))
	require.NoError(t, local.StoreState("ref", &Ref{Head: bar.ID()}))
	require.NoError(t, remote.Store(foo))
	require.NoError(t, remote.Store(baz))
	require.NoError(t, remote.StoreState("ref", &Ref{Head: baz.ID()}))

	ctx, cancel := context.WithCancel(context.Background())
	t.Cleanup(cancel)

	err := Sync(ctx, local, NewLocalDepot(object.ID{1}, remote), "ref")
	require.NoError(t, err)

	_, _, graph, err := loadRefGraph(local, "ref", false)
	require.NoError(t, err)
	require.Len(t, graph.roots, 1)
	require.Len(t, graph.leaves, 1)
	require.Empty(t, graph.orphans)
	require.True(t, graph.seen[foo.ID()])
	require.True(t, graph.seen[bar.ID()])
	require.True(t, graph.seen[baz.ID()])

	var headID object.ID
	for headID = range graph.leaves {
	}
	head, err := object.LoadAs[*Message](local, headID[:])
	require.NoError(t, err)
	require.Len(t, head.Parent, 2)
	require.ElementsMatch(t, head.Parent, []object.ID{
		bar.ID(),
		baz.ID(),
	})
}
