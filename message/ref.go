package message

import (
	"crypto"

	"gitlab.com/firelizzard/rmxp/object"

	"gitlab.com/firelizzard/rmxp/smp"
)

type Ref struct {
	base object.Base
	Head object.ID
}

var typeRef = "ref"

func init() { object.RegisterType[*Ref](typeRef) }

func (e *Ref) ID() object.ID                    { return e.base.ID(e) }
func (e *Ref) Sender() (object.ID, bool)        { return e.base.Sender() }
func (e *Ref) Sign(key crypto.PrivateKey) error { return e.base.Sign(e, key) }

func (e *Ref) MarshalSMP() (*smp.Message, error) {
	type E Ref
	return e.base.Marshal((*E)(e), typeRef)
}

func (e *Ref) UnmarshalSMP(m *smp.Message) error {
	*e = Ref{}
	type E Ref
	return e.base.Unmarshal(m, (*E)(e), typeRef)
}
