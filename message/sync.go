package message

import (
	"bytes"
	"context"
	"errors"
	"fmt"
	"slices"
	"time"

	"gitlab.com/firelizzard/rmxp/object"
)

func Sync(ctx context.Context, store object.Store, remote Depot, refName string) error {
	// Load the local ref state
	ref, thread, graph, err := loadRefGraph(store, refName, false)
	if err != nil {
		return err
	}

	// Do we have a ref for this remote?
	if (remote.ID() == object.ID{}) {
		return object.ErrInvalid.With("remote has no ID")
	}
	remoteRefKey := fmt.Sprintf("remote/%v/%s", remote.ID(), refName)
	remoteRef, err := object.LoadStateAs[*RemoteRef](store, remoteRefKey)
	var base object.ID
	switch {
	case err == nil:
		base = remoteRef.Head
	case !errors.Is(err, object.ErrNotFound):
		return err
	default:
		remoteRef = new(RemoteRef)

		// Determine the merge base
		args := MergeBaseArgs{Ref: refName}
		for _, msg := range thread {
			args.Have = append(args.Have, msg.ID())
		}
		base, err = remote.MergeBase(ctx, args)
		if err != nil {
			return err
		}
	}

	// Ask for updates
	since, err := remote.Since(ctx, SinceArgs{Ref: refName, Head: base})
	if err != nil {
		return err
	}

	var want []object.ID
	for _, id := range since {
		if !graph.seen[id] {
			want = append(want, id)
		}
	}

	// Pull new messages
	if len(want) > 0 {
		msgs, err := remote.Pull(ctx, PullArgs{Message: want})
		if err != nil {
			return err
		}
		for _, msg := range msgs {
			graph.add(msg)
		}
		if len(graph.orphans) > 0 {
			return graph.errOrphans("remote sent us an orphan")
		}
		for _, msg := range msgs {
			err = store.Store(msg)
			if err != nil {
				return err
			}
		}
	}

	// Determine the new merge base
	since, err = graph.since(base)
	if err != nil {
		return err
	}
	base, err = remote.MergeBase(ctx, MergeBaseArgs{
		Ref:   refName,
		Since: base,
		Have:  since,
	})

	// Update the remote ref
	if remoteRef.Head != base {
		remoteRef.Head = base
		err = store.StoreState(remoteRefKey, remoteRef)
		if err != nil {
			return err
		}
	}

	// Is a merge necessary?
	if head, ok := graph.head(); ok {
		ref.Head = head
		return store.StoreState(refName, ref)
	}

	head := graph.merge()
	err = store.Store(head)
	if err != nil {
		return err
	}

	ref.Head = head.ID()
	return store.StoreState(refName, ref)
}

func (g *graph) head() (object.ID, bool) {
	switch len(g.leaves) {
	case 0:
		panic("no leaves!?")

	case 1:
		for id := range g.leaves {
			return id, true
		}
		panic("unreachable")

	default:
		return object.ID{}, false
	}
}

func (g *graph) merge() *Message {
	if len(g.leaves) < 2 {
		panic("invalid merge")
	}
	head := new(Message)
	head.Timestamp = time.Now()
	for id := range g.leaves {
		head.Parent = append(head.Parent, id)
	}
	slices.SortFunc(head.Parent, func(a, b object.ID) int {
		return bytes.Compare(a[:], b[:])
	})
	id := head.ID()
	for _, pid := range head.Parent {
		g.addEdge(id, pid)
	}
	if len(g.leaves) != 1 {
		panic("failed to create a merge message")
	}
	return head
}
