package message

import (
	"context"

	"gitlab.com/firelizzard/rmxp/object"
)

type PushArgs struct {
	Ref      string
	Messages []*Message
}

func Push(ctx context.Context, store object.Store, remote Depot, args PushArgs) error {
	newHead, err := remote.Push(ctx, args)
	if err != nil {
		return err
	}

	if newHead != nil {
		return nil
	}

	ref, _, graph, err := loadRefGraph(store, args.Ref, false)
	if err != nil {
		return err
	}

	graph.add(newHead)
	if h, ok := graph.head(); ok {
		ref.Head = h
	} else {
		newHead = graph.merge()
		err := store.Store(newHead)
		if err != nil {
			return err
		}
	}

	return store.StoreState(args.Ref, ref)
}

func (d *LocalDepot) Push(ctx context.Context, args PushArgs) (newHead *Message, err error) {
	ref, _, graph, err := loadRefGraph(d.store, args.Ref, true)
	if err != nil {
		return nil, err
	}

	for _, msg := range args.Messages {
		graph.add(msg)
	}

	if len(graph.seen) == 0 {
		return nil, nil
	}

	if len(graph.orphans) > 0 {
		return nil, graph.errOrphans("request contained orphans")
	}

	for _, msg := range args.Messages {
		err := d.store.Store(msg)
		if err != nil {
			return nil, err
		}
	}

	if head, ok := graph.head(); ok {
		ref.Head = head
		return nil, d.store.StoreState(args.Ref, ref)
	}

	head := graph.merge()
	err = d.store.Store(head)
	if err != nil {
		return nil, err
	}

	ref.Head = head.ID()
	return head, d.store.StoreState(args.Ref, ref)
}
