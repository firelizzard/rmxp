package message

import (
	"bytes"
	"slices"

	"gitlab.com/firelizzard/rmxp/object"
)

type graph struct {
	parents  map[object.ID][]object.ID
	children map[object.ID][]object.ID
	roots    map[object.ID]bool
	leaves   map[object.ID]bool
	seen     map[object.ID]bool
	orphans  map[object.ID]bool
}

func newGraph() *graph {
	return &graph{
		parents:  map[object.ID][]object.ID{},
		children: map[object.ID][]object.ID{},
		roots:    map[object.ID]bool{},
		leaves:   map[object.ID]bool{},
		seen:     map[object.ID]bool{},
		orphans:  map[object.ID]bool{},
	}
}

func (g *graph) add(msg *Message) {
	id := msg.ID()
	if len(msg.Parent) == 0 {
		g.addRoot(id)
	}
	for _, pid := range msg.Parent {
		g.addEdge(id, pid)
	}
}

func (g *graph) addRoot(id object.ID) {
	g.roots[id] = true
	if len(g.children[id]) == 0 {
		g.leaves[id] = true
	}
	g.seen[id] = true
	delete(g.orphans, id)
}

func (g *graph) addEdge(child, parent object.ID) {
	if child == parent {
		panic("message cannot be its own parent")
	}

	delete(g.leaves, parent)
	if len(g.children[child]) == 0 {
		g.leaves[child] = true
	}

	g.parents[child] = append(g.parents[child], parent)
	g.children[parent] = append(g.children[parent], child)

	g.seen[child] = true
	delete(g.orphans, child)
	if !g.seen[parent] {
		g.orphans[parent] = true
	}
}

func (g *graph) errOrphans(message string) *object.Error {
	var ids []object.PartialID
	for id := range g.orphans {
		ids = append(ids, id[:])
	}
	slices.SortFunc(ids, func(a, b object.PartialID) int {
		return bytes.Compare(a, b)
	})
	return &object.Error{
		Code:    object.ErrInvalid,
		IDs:     ids,
		Message: message,
	}
}
