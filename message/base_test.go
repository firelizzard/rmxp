package message

import (
	"context"
	"fmt"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/firelizzard/rmxp/object"
)

func TestMergeBase(t *testing.T) {
	foo := &Message{Content: "foo"}
	bar := &Message{Content: "bar", Parent: []object.ID{foo.ID()}}
	baz := &Message{Content: "baz", Parent: []object.ID{foo.ID()}}
	bat := &Message{Content: "bat", Parent: []object.ID{bar.ID(), baz.ID()}}

	allMsgs := []*Message{foo, bar, baz, bat}
	allIDs := []object.ID{foo.ID(), bar.ID(), baz.ID(), bat.ID()}
	lup := map[object.ID]string{
		{}:       "none",
		foo.ID(): foo.Content,
		bar.ID(): bar.Content,
		baz.ID(): baz.Content,
		bat.ID(): bat.Content,
	}
	_, _ = allMsgs, allIDs

	cases := []struct {
		Messages []*Message
		Have     []object.ID
		Base     object.ID
	}{
		{Messages: allMsgs, Have: allIDs, Base: bat.ID()},

		{Messages: []*Message{foo}, Have: allIDs, Base: foo.ID()},
		{Messages: []*Message{foo, bar}, Have: allIDs, Base: bar.ID()},
		{Messages: []*Message{foo, baz}, Have: allIDs, Base: baz.ID()},

		{Messages: allMsgs, Have: []object.ID{foo.ID()}, Base: foo.ID()},
		{Messages: allMsgs, Have: []object.ID{foo.ID(), bar.ID()}, Base: foo.ID()},
		{Messages: allMsgs, Have: []object.ID{foo.ID(), baz.ID()}, Base: foo.ID()},

		{Messages: allMsgs, Have: []object.ID{}, Base: object.ID{}},
		{Messages: nil, Have: allIDs, Base: object.ID{}},
	}

	for i, c := range cases {
		t.Run(fmt.Sprintf("Case %d", i), func(t *testing.T) {
			remote := new(object.Memstore)
			for _, msg := range c.Messages {
				require.NoError(t, remote.Store(msg))
			}
			if len(c.Messages) > 0 {
				require.NoError(t, remote.StoreState("ref", &Ref{Head: c.Messages[len(c.Messages)-1].ID()}))
			}

			ctx, cancel := context.WithCancel(context.Background())
			t.Cleanup(cancel)

			resp, err := (&LocalDepot{store: remote}).MergeBase(ctx, MergeBaseArgs{
				Ref:  "ref",
				Have: c.Have,
			})
			require.NoError(t, err)
			require.NotZero(t, lup[resp])
			require.Equal(t, lup[c.Base], lup[resp])
		})
	}
}
