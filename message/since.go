package message

import (
	"context"
	"fmt"

	"gitlab.com/firelizzard/rmxp/object"
)

type SinceArgs struct {
	Ref  string
	Head object.ID
}

func Since(ctx context.Context, store object.Store, remote Depot, args SinceArgs) ([]object.ID, error) {
	if (args.Head == object.ID{}) {
		if (remote.ID() == object.ID{}) {
			return nil, object.ErrInvalid.With("remote has no ID")
		}
		ref, err := object.LoadStateAs[*RemoteRef](store, fmt.Sprintf("remote/%v/%s", remote.ID(), args.Ref))
		if err != nil {
			return nil, err
		}
		args.Head = ref.Head
	}

	return remote.Since(ctx, args)
}

func (d *LocalDepot) Since(ctx context.Context, args SinceArgs) ([]object.ID, error) {
	_, _, graph, err := loadRefGraph(d.store, args.Ref, false)
	if err != nil {
		return nil, err
	}

	return graph.since(args.Head)
}

func (g *graph) since(head object.ID) ([]object.ID, error) {
	if (head != object.ID{}) && !g.seen[head] {
		return nil, object.ErrNotFound.WithFormat("message %s not found", head)
	}

	ids := []object.ID{head}
	seen := map[object.ID]bool{head: true}
	var pos int
	for pos < len(ids) {
		check := ids[pos:]
		pos = len(ids)
		for _, id := range check {
			for _, id := range g.children[id] {
				seen[id] = true
				ids = append(ids, id)
			}
		}
	}
	return ids, nil
}
