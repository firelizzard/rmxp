package message

import (
	"crypto"

	"gitlab.com/firelizzard/rmxp/object"
	"gitlab.com/firelizzard/rmxp/smp"
)

type RemoteRef struct {
	base object.Base
	Head object.ID
}

var typeRemoteRef = object.RegisterType[*RemoteRef]("remoteRef")

func (e *RemoteRef) ID() object.ID                    { return e.base.ID(e) }
func (e *RemoteRef) Sender() (object.ID, bool)        { return e.base.Sender() }
func (e *RemoteRef) Sign(key crypto.PrivateKey) error { return e.base.Sign(e, key) }

func (e *RemoteRef) MarshalSMP() (*smp.Message, error) {
	type E RemoteRef
	return e.base.Marshal((*E)(e), typeRemoteRef)
}

func (e *RemoteRef) UnmarshalSMP(m *smp.Message) error {
	*e = RemoteRef{}
	type E RemoteRef
	return e.base.Unmarshal(m, (*E)(e), typeRemoteRef)
}
